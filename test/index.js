import expect from 'expect';
import Game from '../shared/game';

describe('Game', () => {
  it('needs to get a locations array as the only argument', () => {
    expect(() => new Game).toThrow();
  });
  it('adds an isSunk to each location point', () => {
    const game = new Game([{x: 1, y: 4}]);
    expect('isSunk' in game.locations[0]).toBeTruthy();
  });
  it('should have a isGameOver property', () => {
    const game = new Game([{x: 1, y: 4}]);
    expect('isGameOver' in game).toBeTruthy();
  });

  describe('#move', () => {
    let game;
    beforeEach(() => game = new Game([{x: 1, y: 1}, {x: 2, y: 2}]));

    it('returns true when hit', () => {
      expect(game.move({ x: 1, y: 1 })).toBeTruthy();
    });
    it('returns false when missed', () => {
      expect(game.move({ x: 4, y: 1 })).toBeFalsy();
    });
    it('keeps track of hits', () => {
      game.move({ x: 1, y: 1 });
      expect(game.hits).toEqual([{ x: 1, y: 1 }]);
    });
    it('keeps track of hits', () => {
      game.move({ x: 4, y: 1 });
      expect(game.misses).toEqual([{ x: 4, y: 1 }]);
    });
    it('sets the isSunk property to true when a location is hit', () => {
      game.move({ x: 1, y: 1 });
      expect(game.locations[0].isSunk).toBeTruthy();
    });
    it('sets the isGameOver when all locations have been sunk', () => {
      game.move({ x: 1, y: 1 });
      game.move({ x: 2, y: 2 });
      expect(game.isGameOver).toBeTruthy();
    });
  });
});
