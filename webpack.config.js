var path = require('path');
var webpack = require('webpack');

var definedPlugins = new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    PORT: JSON.stringify(process.env.PORT),
  },
});

var plugins;
var entry;
var devtools;
if (process.env.NODE_ENV !== 'production') {
  plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    definedPlugins,
  ];
  entry = ['webpack-hot-middleware/client', './client/index'];
  devtools = 'eval';
} else {
  plugins = [
    definedPlugins,
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({compressor: { warnings: false }})
  ];
  entry = ['./client/index'];
}

module.exports = {
  devtool: devtools,
  entry: entry,
  output: {
    path: path.join(__dirname, 'static'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: plugins,
  resolve: {
    alias: {
      'react': 'react-lite',
      'react-dom': 'react-lite'
    },
    extensions: ['', '.js']
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel-loader'],
      exclude: /node_modules/
    }]
  }
};
