var spawn = require('child_process').spawn;
var path = require('path');

var args = [path.join(__dirname, 'server.js')];

spawn('npm', ['run', 'build'], { stdio: 'inherit' })
spawn('babel-node', args, { stdio: 'inherit' })
