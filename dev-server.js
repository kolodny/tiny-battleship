var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.config.js');

var express = require('express');
var app = new express();
var port = process.env.SERVER_PORT || 3000;
var host = process.env.SERVER_HOST || 'localhost';

var compiler = webpack(config);
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
app.use(webpackHotMiddleware(compiler));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/static/index.html');
});
app.get('/static/*', function(req, res) {
  console.log(req.url);
  if (/\.hot-update\.json$/.test(req.url)) return;
  res.sendFile(__dirname + req.url.replace(/\.\./g, '.'));
});
app.get(/^\/[\w-]+(\/[\w-]+)*$/, function(req, res) {
  console.log(req.url);
  if (/\.hot-update\.json$/.test(req.url)) return;
  if (/^\/_/.test(req.url)) return;
  res.sendFile(__dirname + '/index.html');
});

app.listen(port, host, function(error) {
  if (error) {
    console.error(error);
  } else {
    console.log('Listening at http://' + host + ':' + port);
  }
});
