import React, { Component } from 'react';
import { Grid } from 'react-bootstrap';
import Screen from './screen';
import HUD from './hud';
import * as api from './api';
import Game from '../shared/game';
global.api = api;
global.Game = Game;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.startNewGame = this.startNewGame.bind(this);
    this.move = this.move.bind(this);
    this.state = {
      myHits: [],
      myMisses: [],
    };
  }
  startNewGame( myLocations ) {
    this.setState({
      game: new Game(myLocations),
      myHits: [],
      myMisses: [],
    });
    api.post('/newgame', { myLocations }).then(response => this.setState({
      gameId: response.gameId,
      gameOverMessage: null,
    }));
  }
  move({ x, y }) {
    const { gameId } = this.state;
    api.put('/move', { gameId, x, y }).then(response => {

      const { isHit, isGameOver } = response;
      const nextState = {};
      const myHitsOrMyMisses = isHit ? 'myHits' : 'myMisses';
      nextState[myHitsOrMyMisses] = this.state[myHitsOrMyMisses].concat({ x, y });

      if (response.isGameOver) {
        nextState.gameOverMessage = 'You win!';
      } else {
        this.state.game.move({ x: response.x, y: response.y });
        if (this.state.game.isGameOver) {
          nextState.gameOverMessage = 'You lose :(';
        }
      }
      this.setState(nextState);
    });
  }
  render() {
    const isReadyToMove = !this.state.gameOverMessage &&
      this.state.game && this.state.game.isGameOver === false;

    return (
      <div>
        <Grid>
          <HUD
            handleNewGame={this.startNewGame}
            handleMove={this.move}
            isReadyToMove={isReadyToMove}
          />
          <Screen
            gameOverMessage={this.state.gameOverMessage}
            pieces={this.state.game && this.state.game.locations}
            misses={this.state.myMisses}
            hits={this.state.myHits}
            isReadyToMove={isReadyToMove}
          />
        </Grid>
        <pre>{JSON.stringify(this.state, null, 2)}</pre>
      </div>
    );
  }
}
