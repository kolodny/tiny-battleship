import React, { Component, PropTypes } from 'react';
import { Alert, Row, Col, Table } from 'react-bootstrap';
import { find } from 'lodash';

const range = x => Array.apply(null, Array(x)).map((_, x) => x);

export default class Screen extends Component {

  renderTd(x, y) {
    const {
      pieces,
      misses,
      hits,
    } = this.props;
    const point = { x, y };
    const classes = [];
    let content = '';
    const piece = find(pieces, point)
    if (piece) {
      content = piece.isSunk ? 'X' : '√';
    }
    if (find(hits, point)) {
      classes.push('hits');
    }
    if (find(misses, point)) {
      classes.push('misses');
    }
    return <td className={classes.join(' ')}>{x},{y} {content}</td>;
  }

  render() {
    const {
      gameOverMessage,
      pieces,
      misses,
      hits,
      isReadyToMove,
    } = this.props;

    if (gameOverMessage) {
      return (
        <Alert>{gameOverMessage}</Alert>
      );
    }

    if (!isReadyToMove) {
      return null;
    }

    return (
      <Row>
        <Col xs={12}>
          <Table>
            <tbody>
              {
                range(5).map(y => {
                  return <tr>
                    {
                      range(5).map(x => {
                        return this.renderTd(x, y)
                      })
                    }
                  </tr>
                })
              }
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

Screen.propTypes = {
  gameOverMessage: PropTypes.string,
  misses: PropTypes.array.isRequired,
  hits: PropTypes.array.isRequired,
  pieces: PropTypes.array,
  isReadyToMove: PropTypes.bool.isRequired,
};
