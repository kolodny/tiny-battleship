import React, { Component, PropTypes } from 'react';
import { Row, Col, ButtonInput, Input } from 'react-bootstrap';

export default class HUD extends Component {
  handleNewGame(e) {
    e.preventDefault();
    const locations = [...this.refs.form.querySelectorAll('input:checked')]
      .map(checkbox => {
        const [x, y] = checkbox.name.split(',').map(Number);
        return { x, y };
      })
    ;
    this.props.handleNewGame(locations);
  }
  handleMove(e, x, y) {
    e.preventDefault();
    this.props.handleMove({ x, y });
  }

  makeGrid(creatorFn) {
    const rows = [];
    for (let y = 0; y < 5; y++) {
      const cols = [];
      for (let x = 0; x < 5; x++) {
        cols.push(<td>{creatorFn(x, y)}</td>);
      }
      rows.push(<tr>{cols}</tr>);
    }
    return <table><tbody>{rows}</tbody></table>;
  }

  render() {
    const {
      handleNewGame,
      handleMove,
      isReadyToMove,
    } = this.props;

    let formContent;
    if (!isReadyToMove) {
      formContent = (
        <div>
          {
            this.makeGrid((x, y) => <input type="checkbox" name={x + ',' + y} />)
          }
          <ButtonInput type="submit" value="New Game" />
        </div>
      );
    } else {
      formContent = (
        this.makeGrid((x, y) => <button onClick={e => this.handleMove(e, x, y)}>{x},{y}</button>)
      );
    }

    return (
      <Row>
        <Col xs={12}>
          <form ref="form" onSubmit={e => this.handleNewGame(e)}>
            {formContent}
          </form>
        </Col>
      </Row>
    );
  }
}

HUD.propTypes = {
  handleNewGame: PropTypes.func.isRequired,
  handleMove: PropTypes.func.isRequired,
  isReadyToMove: PropTypes.bool.isRequired,
}
