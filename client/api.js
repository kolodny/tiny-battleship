const hostname = '//' + location.hostname;
const baseUrl = hostname;

const createMethod = method => (url, payload) => {
  const options = {
    method,
    mode: 'cors',
    headers: {'Content-Type': 'application/json'},
  };

  if (['post', 'put'].indexOf(method) !== -1) {
    options.body = JSON.stringify(payload);
  }

  return fetch(baseUrl + url, options).then(response => response.json());
};

export const post = createMethod('post');
export const put = createMethod('put');
