import { find } from 'lodash';

export default class Game {
  constructor(locations) {
    this.locations = locations.map(({ x, y }) => ({ x, y, isSunk: false }));
    this.isGameOver = false;
    this.hits = [];
    this.misses = [];
  }
  move({ x, y }) {
    const found = find(this.locations, { x, y });
    if (found) {
      found.isSunk = true;
      this.hits.push({ x, y });
    } else {
      this.misses.push({ x, y });
    }
    if (!find(this.locations, {isSunk: false})) {
      this.isGameOver = true;
    }
    return !!found;
  }
}
