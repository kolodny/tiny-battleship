import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { find } from 'lodash';
import Game from './shared/game';
const app = express();
app.use(cors());
app.use(bodyParser.json());

const port = process.env.PORT || 5000;
const games = {};
const getRandomLocation = () => ({ x: ~~(Math.random() * 5), y: ~~(Math.random() * 5) });

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/static/index.html');
});
app.use('/static', express.static('static'));

app.post('/newgame', function (req, res) {
  const gameId = Math.random().toString(36).slice(2);
  const locations = [];
  let nextLocation;
  for (let i = 0; i < 5; i++) {
    do {
      nextLocation = getRandomLocation();
    } while (find(locations, nextLocation));
    locations.push(nextLocation);
  }
  games[gameId] = new Game(locations);
  console.log('started new game', gameId, locations.map(({x, y}) => [x, y]));
  res.json({ gameId });
});

app.put('/move', (req, res) => {
  const { gameId, x, y } = req.body;
  const game = games[gameId];
  const isHit = game.move({ x, y });
  let cpuX;
  let cpuY;
  if (!game.isGameOver) {
    cpuX = Math.floor(Math.random() * 5);
    cpuY = Math.floor(Math.random() * 5);
  }
  console.log({ gameId, x, y, isHit, isGameOver: game.isGameOver, cpuX, cpuY })
  res.send({ isHit, isGameOver: game.isGameOver, x: cpuX, y: cpuY });
});

const server = app.listen(port, () => {
  const host = server.address().address;
  const port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
